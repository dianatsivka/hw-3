const { Truck } = require('./Truck');

const createTruck = async (req, res, next) => {
  try {
  const truck = new Truck({
    created_by: req.user.userId,
    assign_to: req.body.assign_to,
    status: req.body.status,
    type: req.body.type
  });

  truck.save().then((saved) => {
    res.json({message: 'created successfully'});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
};

const getAllTrucks = async (req, res, next) => {
  try {
  await Truck.find({created_by: req.user.userId}).then((result) => {
    res.json({trucks: result});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const updateTruck = async (req, res, next) => {
  const { status, type } = req.body;
  return Truck.findByIdAndUpdate(req.params.id, {$set: { status, type } })
    .then((truck) => {
      res.json({message: 'updated'});
    });
};

const deleteTruck = async (req, res, next) => Truck.findByIdAndDelete(req.params.id)
  .then((truck) => {
    res.json({message: 'deleted'});
  });

const assignTruck = async (req, res, next) => {
  return Truck.findByIdAndUpdate(req.params.id, {$set: { assign_to: req.user.userId} })
    .then((truck) => {
      res.json({message: 'assigned'});
    });
}

module.exports = {
  createTruck,
  getAllTrucks,
  updateTruck,
  deleteTruck,
  assignTruck
};