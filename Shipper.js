const mongoose = require('mongoose');

const Shipper = mongoose.model('shippers', {
  username: {
    type: String,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role:{
    type: String,
    default: 'SHIPPER'
  }
});


module.exports = {
  Shipper
};