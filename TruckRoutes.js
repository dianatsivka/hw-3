const { Truck } = require('./Truck');
const express = require('express');
const { createTruck, getAllTrucks, updateTruck, deleteTruck, assignTruck } = require('./TruckFunctions');
const { authMiddleware } = require('./authMiddleware');
const router = express.Router();


router.post('/', authMiddleware, createTruck)

router.get('/', authMiddleware, getAllTrucks)

router.put('/:id', updateTruck)

router.post('/:id/assign',authMiddleware, assignTruck)

router.delete('/:id', deleteTruck)


module.exports = {
  TruckRoutes: router,
}