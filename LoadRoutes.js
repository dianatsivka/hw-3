const { Load } = require('./Load');
const express = require('express');
const router = express.Router();
const { createLoad, getAllLoads, updateLoad, deleteLoad, assignLoad, getAllActiveLoads, getLoadById, getLoadShipInfo, postLoad, iterateLoad } = require('./loadFunctions');
const { authMiddleware } = require('./authMiddleware');


router.post('/',authMiddleware, createLoad)

router.post('/:id/post', postLoad)

router.get('/',authMiddleware, getAllLoads)

router.get('/active',authMiddleware, getAllActiveLoads)

router.get('/:id', getLoadById)

router.get('/:id/shipping_info', getLoadShipInfo)

router.put('/:id', updateLoad)

router.patch('/:id',authMiddleware, assignLoad)

router.patch('/active/state', iterateLoad)

router.delete('/:id', deleteLoad)


module.exports = {
  LoadRoutes: router,
};