const express = require('express');
const mongoose = require('mongoose');


const app = express();


mongoose.connect('mongodb+srv://dianatsivka:epamlab2022@cluster1.e9ikb2y.mongodb.net/?retryWrites=true&w=majority')
.then(() => {
  console.log('MongoDB connected')
})
.catch(err => {
  console.log(err)
})


app.use(express.json());

const { UserRoutes } = require('./UserRoutes');
app.use('/api/auth', UserRoutes);
const { TruckRoutes } = require('./TruckRoutes');
app.use('/api/trucks', TruckRoutes);
const { LoadRoutes } = require('./LoadRoutes');
app.use('/api/loads', LoadRoutes);


const PORT = process.env.PORT || 8080;
app.listen(PORT, console.log(`Server is running on port ${PORT}`));