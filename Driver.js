const mongoose = require('mongoose');

const Driver = mongoose.model('drivers', {
  username: {
    type: String,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role:{
    type: String,
    default: 'DRIVER'
  }
})

module.exports = {
  Driver
};