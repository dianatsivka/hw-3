const { Driver } = require('./Driver');
const express = require('express');
const { registerDriver, loginDriver, changePassword, getProfileInfo, deleteProfile } = require('./DriverFunctions');
const router = express.Router();

router.post('/register', registerDriver)

router.post('/login', loginDriver)

router.patch('/me', changePassword)

router.get('/me', getProfileInfo)

router.delete('/me', deleteProfile)


module.exports = {
  DriverRoutes: router
}