const { Shipper } = require('./Shipper');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerShipper = async (req, res, next) => {
  try {
  const { email, password } = req.body;

  const shipper = new Shipper({
    email,
    password: await bcrypt.hash(password, 10)
  });

  shipper.save()
  .then(saved => res.json({message: `successfully created driver ${shipper}`}))
  .catch(err => {
    next(err)
  });
  } catch (error) {
    res.status(404).json('Server error register')
  }
}

const loginShipper = async (req, res, next) => {
    const shipper = await Shipper.findOne({ email: req.body.email });
  if (shipper && await bcrypt.compare(String(req.body.password), String(shipper.password))){
    const payload = { email: shipper.email, userId: shipper._id };
    const jwt_token = jwt.sign(payload, 'secret-jwt-key');
    return res.json({message: `login successfully`, jwt_token})
  }else{
    return res.status(400).json({'message': `${shipper}`});
  }
}

const changePassword = async (req, res, next) => {
  const shipper = await Shipper.findById(req.shipper.userId)
  const { oldPassword, newPassword } = req.body;
  if(await bcrypt.compare(String(oldPassword), String(shipper.password))){
    return Shipper.findByIdAndUpdate(req.shipper.userId, {$set: { password: await bcrypt.hash( newPassword, 10 ) } })
    .then((shipper) => {
      res.json({message: "password changed"});
    });
  }
}

const getProfileInfo = async (req, res, next) => {
  const shipper = {
    "email": `${req.shipper.email}`,
    "userId": `${req.shipper.userId}`
  }
  if(shipper){
    return res.json({shipper})
  }
}

const deleteProfile = (req, res) => User.findByIdAndDelete(req.shipper.userId)
.then((shipper) => {
  res.json({message: "delete"});
})


module.exports = {
  registerShipper,
  loginShipper,
  changePassword, 
  getProfileInfo,
  deleteProfile
};