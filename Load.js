const mongoose = require('mongoose');


const Load = mongoose.model('loads', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
  },
  assign_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  status: {
    type: String,
    default: 'NEW'
  },
  state: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String
  },
  delivery_address: {
    type: String
  },
  dimentions: {
    width: Number,
    height: Number,
    length: Number
  },
  driver_found: Boolean

});

module.exports = {
  Load
};