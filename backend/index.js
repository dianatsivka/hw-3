const express = require('express');
const mongoose = require('mongoose');


const app = express();

mongoose.connect('mongodb+srv://dianatsivka:epamlab2022@taskmanager.dhafjjj.mongodb.net/?retryWrites=true&w=majority')
.then(() => {
  console.log('MongoDB connected')
})
.catch(err => {
  console.log(err)
})

app.use(express.json());

const { DriverRoutes } = require('./routes/DriverRoutes');
app.use('/api', DriverRoutes);
const { ShipperRoutes } = require('./routes/ShipperRoutes');
app.use('/api', ShipperRoutes);

const PORT = process.env.PORT || 8080;
app.listen(PORT, console.log(`Server is running on port ${PORT}`));