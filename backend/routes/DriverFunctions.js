const { Driver } = require('../models/Driver');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerDriver = async (req, res, next) => {
  try {
  const { username, password } = req.body;

  const driver = new Driver({
    username,
    password: await bcrypt.hash(password, 10)
  });

  driver.save()
  .then(saved => res.json({message: `successfully created driver ${driver}`}))
  .catch(err => {
    next(err)
  })
  } catch (error) {
    res.status(404).json('Server error register')
  }
}

const loginDriver = async (req, res, next) => {
    const driver = await Driver.findOne({ username: req.body.username });
  if (driver && await bcrypt.compare(String(req.body.password), String(driver.password))){
    const payload = { username: driver.username, userId: driver._id };
    const jwt_token = jwt.sign(payload, 'secret-jwt-key');
    return res.json({message: `login successfully`, jwt_token})
  }else{
    return res.status(400).json({'message': `${driver}`});
  }
}

const changePassword = async (req, res, next) => {
  const driver = await Driver.findById(req.driver.userId)
  const { oldPassword, newPassword } = req.body;
  if(await bcrypt.compare(String(oldPassword), String(driver.password))){
    return Driver.findByIdAndUpdate(req.driver.userId, {$set: { password: await bcrypt.hash( newPassword, 10 ) } })
    .then((driver) => {
      res.json({message: "password changed"});
    });
  }
}

const getProfileInfo = async (req, res, next) => {
  const driver = {
    "username": `${req.driver.username}`,
    "userId": `${req.driver.userId}`
  }
  if(driver){
    return res.json({driver})
  }
}

const deleteProfile = (req, res) => User.findByIdAndDelete(req.driver.userId)
.then((driver) => {
  res.json({message: "delete"});
})


module.exports = {
  registerDriver,
  loginDriver,
  changePassword, 
  getProfileInfo,
  deleteProfile
};