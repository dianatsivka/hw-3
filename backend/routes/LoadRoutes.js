const { Load } = require('../models/Load');
const express = require('express');
const router = express.Router();
const { createload, getAllLoads, updateLoad, deleteLoad, assignLoad } = require('./noteService.js');


router.post('/', createLoad)

router.get('/', getAllLoads)

router.put('/:id', updateLoad)

router.patch('/:id', assignLoad)

router.delete('/:id', deleteLoad)

module.exports = {
  loadRouter: router,
};