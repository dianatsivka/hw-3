const { Truck } = require('../models/Truck');
const express = require('express');
const router = express.Router();
const { createTruck, getAllTrucks, updateTruck, deleteTruck, assignTruck } = require('./noteService.js');


router.post('/', createTruck)

router.get('/', getAllTrucks)

router.put('/:id', updateTruck)

router.patch('/:id', assignTruck)

router.delete('/:id', deleteTruck)

module.exports = {
  trucksRouter: router,
};