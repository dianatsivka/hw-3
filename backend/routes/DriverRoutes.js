const express = require('express');
const { Driver } = require('../models/Driver');
const { registerDriver, loginDriver, changePassword, getProfileInfo, deleteProfile } = require('./DriverFunctions');
const router = express.Router();

router.post('/driver/register', registerDriver)

router.post('/driver/login', loginDriver)

router.patch('/driver/me', changePassword)

router.get('/driver/me', getProfileInfo)

router.delete('/driver/me', deleteProfile)

module.exports = {
  DriverRoutes: router,
};