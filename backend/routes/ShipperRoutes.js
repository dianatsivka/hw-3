const express = require('express');
const { Shipper } = require('../models/Shipper');
const { registerShipper, loginShipper, changePassword, getProfileInfo, deleteProfile } = require('./ShipperFunctions');
const router = express.Router();

router.post('/shipper/register', registerShipper)

router.post('/shipper/login', loginShipper)

router.patch('/shipper/me', changePassword)

router.get('/shipper/me', getProfileInfo)

router.delete('/shipper/me', deleteProfile)

module.exports = {
  ShipperRoutes: router,
};