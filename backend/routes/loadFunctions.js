const { Load } = require('../models/Load');

const createLoad = (req, res, next) => {
  try {
  const load = new Load({
    created_by: req.body.created_by,
    assign_to: req.user.assign_to,
    status: req.body.status,
    type: req.body.type
  });

  load.save().then((saved) => {
    res.json({message: 'created successfully'});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const getAllLoads = (req, res, next) => {
  try {
  Load.find({created_by: req.shipper._id}).then((result) => {
    res.json({message: result});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const updateLoad = (req, res, next) => {
  const { status, type } = req.body;
  return Load.findByIdAndUpdate(req.params.id, {$set: { status, type } })
    .then((load) => {
      res.json({message: 'updated'});
    });
};

const deleteLoad = (req, res, next) => Load.findByIdAndDelete(req.params.id)
  .then((Load) => {
    res.json({message: 'deleted'});
  });

const assignLoad = (req, res, next) => {
  return Load.findByIdAndUpdate(req.params.id, {$set: { assign_to: req.driver._id} })
    .then((Load) => {
      res.json({message: 'assigned'});
    });
}

module.exports = {
  createLoad,
  getAllLoads,
  updateLoad,
  deleteLoad,
  assignLoad
};