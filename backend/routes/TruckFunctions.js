const { Truck } = require('../models/Truck');

const createTruck = (req, res, next) => {
  try {
  const truck = new Truck({
    created_by: req.body.created_by,
    assign_to: req.user.assign_to,
    status: req.body.status,
    type: req.body.type
  });

  truck.save().then((saved) => {
    res.json({message: 'created successfully'});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const getAllTrucks = (req, res, next) => {
  try {
  Truck.find({created_by: req.driver._id}).then((result) => {
    res.json({notes: result});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const updateTruck = (req, res, next) => {
  const { status, type } = req.body;
  return Truck.findByIdAndUpdate(req.params.id, {$set: { status, type } })
    .then((truck) => {
      res.json({message: 'updated'});
    });
};

const deleteTruck = (req, res, next) => Truck.findByIdAndDelete(req.params.id)
  .then((truck) => {
    res.json({message: 'deleted'});
  });

const assignTruck = (req, res, next) => {
  return Truck.findByIdAndUpdate(req.params.id, {$set: { assign_to: req.driver._id} })
    .then((truck) => {
      res.json({message: 'assigned'});
    });
}

module.exports = {
  createTruck,
  getAllTrucks,
  updateTruck,
  deleteTruck,
  assignTruck
};