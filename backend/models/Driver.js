const mongoose = require('mongoose');

const Driver = mongoose.model('drivers', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  Driver
};