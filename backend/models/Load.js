const mongoose = require('mongoose');

const Load = mongoose.model('loads', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assign_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  status: {
    type: string,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String
  },
  delivery_address: {
    type: String
  },
  dimentions: {
    width: Number,
    height: Number,
    length: Number
  }

});

module.exports = {
  Load
};