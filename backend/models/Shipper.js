const mongoose = require('mongoose');

const Shipper = mongoose.model('shippers', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  Shipper
};