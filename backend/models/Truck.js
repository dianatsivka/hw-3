const mongoose = require('mongoose');

const Truck = mongoose.model('trucks', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assign_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  status: {
    type: string,
    required: true,
  },
  type: {
    type: string,
    required: true,
  }

});

module.exports = {
  Truck
};