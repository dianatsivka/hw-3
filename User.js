const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role:{
    type: String,
  }
})

module.exports = {
  User
};