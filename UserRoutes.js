const { User } = require('./User');
const express = require('express');
const { registerUser, loginUser, changePassword, getProfileInfo, deleteProfile } = require('./UserFunctions');
const router = express.Router();

router.post('/register', registerUser)

router.post('/login', loginUser)

router.patch('/me', changePassword)

router.get('/me', getProfileInfo)

router.delete('/me', deleteProfile)


module.exports = {
  UserRoutes: router
}