const { Load } = require('./Load');

const createLoad = (req, res, next) => {
  try {
  const load = new Load({
    created_by: req.user.userId,
    assign_to: req.body.assign_to,
    status: req.body.status,
    type: req.body.type,
    name: req.body.name
  });

  load.save().then((saved) => {
    res.json({message: 'created successfully'});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const postLoad = (req, res, next) => {
  return Load.findByIdAndUpdate(req.params.id, {$set: { driver_found: true } })
    .then((load) => {
      res.json({message: {driver_found: true}});
    });
}

const getAllLoads = (req, res, next) => {
  try {
  Load.find({created_by: req.user.userId}).then((result) => {
    res.json({loads: result});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const getAllActiveLoads = (req, res, next) => {
  try {
  Load.find({created_by: req.user.userId, status: NEW}).then((result) => {
    res.json({loads: result});
  });
  } catch (error) {
    res.status(404).json('Server error')
  }
}

const getLoadById = (req, res, next) => {
  return Load.findById(req.params.id)
    .then((load) => {
      res.json({load: load});
    });
};

const getLoadShipInfo = (req, res, next) => {
  return Load.findById(req.params.id)
    .then((load) => {
      res.json({load: load});
    });
}

const iterateLoad = (req, res, next) => {
  return Load.findByIdAndUpdate(req.params.id, {$set: { state: 'hhh' } })
    .then((load) => {
      res.json({message: 'updated'});
    });
}

const updateLoad = (req, res, next) => {
  const { status, type } = req.body;
  return Load.findByIdAndUpdate(req.params.id, {$set: { status, type } })
    .then((load) => {
      res.json({message: 'updated'});
    });
};

const deleteLoad = (req, res, next) => Load.findByIdAndDelete(req.params.id)
  .then((Load) => {
    res.json({message: 'deleted'});
  });

const assignLoad = (req, res, next) => {
  return Load.findByIdAndUpdate(req.params.id, {$set: { assign_to: req.user.userId, status: 'ASSIGNED'} })
    .then((Load) => {
      res.json({message: 'assigned'});
    });
}


module.exports = {
  createLoad,
  getAllLoads,
  updateLoad,
  deleteLoad,
  assignLoad,
  getAllActiveLoads, 
  getLoadById, 
  getLoadShipInfo, 
  postLoad, 
  iterateLoad
};