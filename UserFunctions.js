const { User } = require('./User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  try {
  const { email, password, role } = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10), 
    role
  });

  user.save()
  .then(saved => res.json({message: `successfully created user ${user}`}))
  .catch(err => {
    next(err)
  });
  } catch (error) {
    res.status(404).json('Server error register')
  }
}

const loginUser = async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))){
    const payload = { email: user.email, userId: user._id, role: user.role };
    const jwt_token = jwt.sign(payload, 'secret-jwt-key');
    return res.json({message: `login successfully`, jwt_token})
  }else{
    return res.status(400).json({'message': `${user}`});
  }
}

const changePassword = async (req, res, next) => {
  const user = await User.findById(req.user.userId)
  const { oldPassword, newPassword } = req.body;
  if(await bcrypt.compare(String(oldPassword), String(user.password))){
    return User.findByIdAndUpdate(req.user.userId, {$set: { password: await bcrypt.hash( newPassword, 10 ) } })
    .then((user) => {
      res.json({message: "password changed"});
    });
  }
}

const getProfileInfo = async (req, res, next) => {
  const user = {
    "email": `${req.user.email}`,
    "userId": `${req.user._id}`
  }
  if(user){
    return res.json({user})
  }
}

const deleteProfile = async (req, res) => User.findByIdAndDelete(req.user.userId)
.then((user) => {
  res.json({message: "delete"});
})


module.exports = {
  registerUser,
  loginUser,
  changePassword, 
  getProfileInfo,
  deleteProfile
};