const express = require('express');
const { Shipper } = require('./Shipper');
const { registerShipper, loginShipper, changePassword, getProfileInfo, deleteProfile } = require('./ShipperFunctions');
const router = express.Router();

router.post('/register', registerShipper)

router.post('/login', loginShipper)

router.patch('/me', changePassword)

router.get('/me', getProfileInfo)

router.delete('/me', deleteProfile)


module.exports = {
  ShipperRoutes: router,
};