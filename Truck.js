const mongoose = require('mongoose');

const Truck = mongoose.model('trucks', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
  },
  assign_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  status: {
    type: String,
    default: 'IS'
  },
  type: {
    type: String,
    required: true,
  }

});


module.exports = {
  Truck
};